package com.example.demo.domain;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter

public class Business {
    private String id;
    private String name;
    private String phone;
    private Address address;
    private String eMail;

    public Business() {
        this.id = UUID.randomUUID().toString();
    }

    public Business(String name, String phone, Address address, String eMail) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.eMail = eMail;
    }
}
