
package com.example.demo.domain;

import org.junit.Assert;
import org.junit.Test;

public class BusinessTest {

    @Test
    public void shouldCreateANewBusinessAccount() {
        //Arrange
        Address address = new Address("a", "b", "c", "d");
        //Act
        Business sut = new Business("name", "phone", address, "e-mail");
        //Assert
        Assert.assertEquals("name", sut.getName());
    }
}
