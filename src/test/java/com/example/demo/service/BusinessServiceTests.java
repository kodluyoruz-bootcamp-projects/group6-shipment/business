package com.example.demo.service;

import com.example.demo.domain.Address;
import com.example.demo.domain.Business;
import com.example.demo.exception.BusinessNotCreatedException;
import com.example.demo.exception.BusinessNotFoundException;
import com.example.demo.repository.BusinessRepository;
import com.example.demo.service.BusinessService;
import lombok.SneakyThrows;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BusinessServiceTests {

    @Mock
    BusinessRepository businessRepository;
    @InjectMocks
    BusinessService businessService;

    @SneakyThrows
    @Test
    public void shouldReturnBusinessNotFoundExceptionWhenThereIsNoBusinessWithId() {
        //Arrange
        String id = "123";
        when(businessRepository.getBusinessAccountByID(id)).thenThrow(new BusinessNotFoundException("Business Account Cannot Found"));
        //Act
        Throwable throwable = catchThrowable(() -> businessService.getBusinessAccountByID(id));
        // Assert
        assertThat(throwable).isInstanceOf(BusinessNotFoundException.class).hasMessage("Business Account Cannot Found");
    }

    @SneakyThrows
    @Test
    public void shouldThrowBusinessNotCreatedExceptionWhenBusinessIsNotValid() {
        //Arrange
        doThrow(BusinessNotCreatedException.class).when(businessRepository).insertBusinessAccount(any(Business.class));
        Business business= new Business();
        //Act
        Throwable throwable = catchThrowable(() -> businessService.createBusinessAccount(business));
        // Assert
        assertThat(throwable).isInstanceOf(BusinessNotCreatedException.class);
    }
    @SneakyThrows
    @Test
    public void shouldThrowBusinessNotFoundExceptionWhenDeleteInvalidBusinessAccount() {
        //Arrange
        String id = "1";
        doThrow(BusinessNotFoundException.class).when(businessRepository).deleteBusinessAccountByID(any(String.class));
        //Act
        Throwable throwable = catchThrowable(() -> businessService.deleteBusinessAccount(id));
        // Assert
        assertThat(throwable).isInstanceOf(BusinessNotFoundException.class);
    }
}
